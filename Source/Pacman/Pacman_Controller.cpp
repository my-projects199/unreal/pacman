// Fill out your copyright notice in the Description page of Project Settings.


#include "Pacman_Controller.h"
#include "PacmanPawn.h"
void APacman_Controller::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Up", IE_Pressed, this, &APacman_Controller::MoveUp);
	InputComponent->BindAction("Down", IE_Pressed, this, &APacman_Controller::MoveDown);
	InputComponent->BindAction("Right", IE_Pressed, this, &APacman_Controller::MoveRight);
	InputComponent->BindAction("Left", IE_Pressed, this, &APacman_Controller::MoveLeft);
}

APacmanPawn* APacman_Controller::GetPacmanPawn() const
{
	return Cast<APacmanPawn>(GetPawn());
}

void APacman_Controller::MoveUp()
{
	if(GetPacmanPawn() != nullptr)
	{
		GetPacmanPawn()->SetDirection(FVector::UpVector);
	}
}

void APacman_Controller::MoveDown()
{
	if(GetPacmanPawn() != nullptr)
	{
		GetPacmanPawn()->SetDirection(FVector::DownVector);
	}
}

void APacman_Controller::MoveRight()
{
	if(GetPacmanPawn() != nullptr)
	{
		GetPacmanPawn()->SetDirection(FVector::RightVector);
	}
}

void APacman_Controller::MoveLeft()
{
	if(GetPacmanPawn() != nullptr)
	{
		GetPacmanPawn()->SetDirection(FVector::LeftVector);
	}
}
