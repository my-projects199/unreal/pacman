// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Sound/SoundCue.h"

#include "TeleportActor.generated.h"

UCLASS()
class PACMAN_API ATeleportActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATeleportActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TeleportToTarget(AActor* OtherActor);
	
	UPROPERTY(EditAnywhere)
	ATeleportActor* Target = nullptr;

	UPROPERTY(EditAnywhere)
	USoundCue* TeleportSound;

	UFUNCTION()
	void OnOverlapBegin(AActor* TeleportActor, AActor* OtherActor);

};
